Rails.application.routes.draw do
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'

  root 'users#new'

  get '/terms',     to: 'static_pages#help'
  get '/signup',    to: 'users#new'
  post '/signup',   to: 'users#create'
  get '/change',    to: 'users#change'
  
  get '/login',     to: 'sessions#new'
  post '/login',    to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  
  resources :users do
    member do
      get :following, :followers, :change
      patch :renew
    end
  end
  
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:new, :create, :destroy, :index]
  resources :relationships,       only: [:create, :destroy]
end
