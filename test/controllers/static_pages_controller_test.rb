require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = "Instagram Clone App"
  end
  
   test "should get root" do
    get root_url
    assert_response :success
  end
end
