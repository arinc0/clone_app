User.create!(name:  "Example User",
             user_name: "Example",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  user_name =Faker::FunnyName.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               user_name: user_name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

30.times do |n|
  content = Faker::Lorem.sentence(word_count: 5)
  picture = File.open("./app/assets/images/picture#{n+1}.jpg")
  user    = User.find_by(id: n+1)
  user.microposts.create!(content: content, picture: picture )
end

user = User.find_by(id: 1)
10.times do |n|
  content = Faker::Lorem.sentence(word_count: 5)
  picture = File.open("./app/assets/images/picture#{n+31}.jpg")
  user.microposts.create!(content: content, picture: picture )
end

  


# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }