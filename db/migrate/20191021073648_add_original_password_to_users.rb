class AddOriginalPasswordToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :original_password, :string
  end
end
